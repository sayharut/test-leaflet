import 'leaflet/dist/leaflet.css'
import L from 'leaflet'

L.AnimatedMarker = L.Marker.extend({
  options: {
    distance: 200,
    interval: 1000,
    autoStart: true,
    onEnd: () => {},
    clickable: false
  },

  initialize(latlngs, options) {
    this.setLine(latlngs)
    L.Marker.prototype.initialize.call(this, latlngs[0], options)
  },

  _chunk (latlngs) {
    let i
    let len = latlngs.length
    let chunkedLatLngs = []

    for (i = 1; i < len; i++) {
      let cur = latlngs[i - 1]
      let next = latlngs[i]
      let dist = cur.distanceTo(next)
      let factor = this.options.distance / dist
      let dLat = factor * (next.lat - cur.lat)
      let dLng = factor * (next.lng - cur.lng)

      if (dist > this.options.distance) {
        while (dist > this.options.distance) {
          cur = new L.LatLng(cur.lat + dLat, cur.lng + dLng)
          dist = cur.distanceTo(next)
          chunkedLatLngs.push(cur)
        }
      } else {
        chunkedLatLngs.push(cur)
      }
    }
    chunkedLatLngs.push(latlngs[len - 1])

    return chunkedLatLngs
  },

  onAdd(map) {
    L.Marker.prototype.onAdd.call(this, map)
    if (this.options.autoStart) {
      this.start()
    }
  },

  animate() {
    let self = this
    let len = this._latlngs.length
    let speed = this.options.interval
    if (this._i < len && this.i > 0) {
      speed = this._latlngs[this._i - 1].distanceTo(this._latlngs[this._i]) / this.options.distance * this.options.interval
    }
    if (L.DomUtil.TRANSITION) {
      if (this._icon) { this._icon.style[L.DomUtil.TRANSITION] = ('all ' + speed + 'ms linear') }
      if (this._shadow) { this._shadow.style[L.DomUtil.TRANSITION] = 'all ' + speed + 'ms linear' }
    }
    this.setLatLng(this._latlngs[this._i])
    this._i++
    this._tid = setTimeout( () => {
      if (self._i === len) {
        self.options.onEnd.apply(self, Array.prototype.slice.call(arguments))
      } else {
        self.animate()
      }
    }, speed)
  },

  start() {
    this.animate()
  },

  stop() {
    if (this._tid) {
      clearTimeout(this._tid)
    }
  },

  setLine(latlngs) {
    if (L.DomUtil.TRANSITION) {
      this._latlngs = latlngs
    } else {
      this._latlngs = this._chunk(latlngs)
      this.options.distance = 10
      this.options.interval = 30
    }
    this._i = 0
  }

})

L.animatedMarker = (latlngs, options) => {
  return new L.AnimatedMarker(latlngs, options)
}

export default L.animatedMarker
